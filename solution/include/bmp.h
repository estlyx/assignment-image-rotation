#include "struct_image.h"
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>
void read_file(const char* fileName, struct image* img);
void write_file(const char* fileName, const struct image* image);
